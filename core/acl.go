package core

import (
    "gorm.io/gorm"
)

type AclLevel int
func (a AclLevel) String() string {
    return AclStrings[int(a)]
}

var AclStrings map[int]string = map[int]string{
    int(AclForbidden): "AclForbidden",
    int(AclView): "AclView",
    int(AclExecute): "AclExecute",
    int(AclEdit): "AclEdit",
    int(AclDelete): "AclDelete",
    int(AclComplete): "AclComplete",
}

var ReverseAclStrings map[string]AclLevel = map[string]AclLevel{
    "AclForbidden": AclForbidden,
    "AclView": AclView,
    "AclExecute": AclExecute,
    "AclEdit": AclEdit,
    "AclDelete": AclDelete,
    "AclComplete": AclComplete,
}

func WebAclCheck ( have AclLevel, want string ) bool {
    wantLevel,ok := ReverseAclStrings[want]
    if ok {
        return (have >= wantLevel)
    } else {
        return false
    }
}

const (
    AclForbidden AclLevel = 0
    AclView      AclLevel = 1
    AclExecute   AclLevel = 2
    AclEdit      AclLevel = 3
    AclDelete    AclLevel = 4
    AclComplete  AclLevel = 5
)

func AclToString ( id int ) string {
    if id > int(AclComplete) || id < int(AclForbidden) {
        return ""
    }
    return AclStrings[id]
}

func AclCheck ( db *gorm.DB, user User, object Repository ) AclLevel {
    if user.ID <= 0 || object.ID <= 0 {
        return AclForbidden
    }
    // Superuser and owner always have complete access
    if user.Superuser || user.ID == object.UserID {
        return AclComplete
    }

    var acls []Acl

    // first check user privileges:
    // forbidden takes precedence; otherwise search for highest membership
    db.Where("user_id = ? AND repository_id = ?", user.ID, object.ID).Find ( &acls )
    var access_level AclLevel = AclForbidden
    for _, v := range acls {
        if v.AccessLevel == AclForbidden {
            return AclForbidden
        }
        if v.AccessLevel > access_level {
            access_level = v.AccessLevel
        }
    }
    if access_level != AclForbidden {
        return access_level
    }

    // then check for groups:
    // forbidden takes precedence; otherwiese search for highest membership
    db.Where("repository_id = ? and group_id IN (?)", object.ID, db.Table("user_groups").Select("group_id").Where("user_id=?", user.ID)).Find( &acls );
    for _, v := range acls {
        if v.AccessLevel == AclForbidden {
            return AclForbidden
        }
        if v.AccessLevel > access_level {
            access_level = v.AccessLevel
        }
    }

    return access_level
}


