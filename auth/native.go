package auth

// native auth provider, i.e. password check for login

import (
    "errors"
    "net/url"

    "codeberg.org/snaums/mvoCI/core"

    "golang.org/x/crypto/bcrypt"
)

var nativeProvider = Provider {
    Name         : "native",
    HumanName    : "Built-In Authentification",
    Description  : "Built-In Authentification based on passwords",

    Verify       : nativeVerify,
    SetSecret    : nativeSetSecret,

    ConfigView   : nativeConfigView,
    ConfigCommit : nativeConfigCommit,

    Cap          : ProviderCap {
        MainEnable      : true,     // can be used as a main authentication module
        Configurable    : true,
    },
}

// all auth providers need to add themselves to the auth provider list
func init () {
    __init ( &nativeProvider )
}

// check the password hashes of the user input and the stored hash
func checkPassword ( u core.User, passwd string ) bool {
    err := bcrypt.CompareHashAndPassword( []byte(u.Passhash), []byte(passwd) )
    if err != nil {
        return false;
    }
    return true;
}

// create the hash of a given plaintext password for storing
func createPasshash ( passwd string ) string {
    h,_ := bcrypt.GenerateFromPassword ( []byte(passwd), 10 );
    return string(h);
}

// no configuration is necessary for the configuration view
func nativeConfigView ( user core.User, prov *core.AuthProvider ) ( map[string]string, error ) {
    return map[string]string{}, nil
}

// Commiting the config is done by checking the two given password boxes (are they the same?)
func nativeConfigCommit ( user *core.User, prov *core.AuthProvider, params url.Values ) error {
    var apasswd []string    // old password
    var ag1, ag2 []string   // two times the new password

    var passwd string
    var g1, g2 string

    apasswd, okold := params["old"]
    ag1, ok1 := params["new1"]
    ag2, ok2 := params["new2"]
    if ok1 == false || ok2 == false || okold == false ||
       len(apasswd) == 0 || len(ag1) == 0 || len(ag2) == 0 {
        errors.New ("An internal error occurred. Please report this bug")
    }

    passwd = apasswd[0]
    g1 = ag1[0]
    g2 = ag2[0]

    if checkPassword ( *user, passwd ) {
        if g1 == g2 {
            user.Passhash = createPasshash ( g1 );
            return nil;
        } else {
            return errors.New ("The passwords do not match")
        }
    } else {
        return errors.New ("Incorrect old password")
    }
    return errors.New("Not reachable")
}

// verify the password against the hash using checkPassword
func nativeVerify (user *core.User, stepExtra string, given string, extra string, _ string ) error {
    if checkPassword ( *user, given ) == true {
        return nil;
    } else {
        return errors.New ("Incorrect login");
    }
}

// set a new secret
func nativeSetSecret ( user *core.User, pass1 string, pass2 string ) error {
    if pass1 != pass2 {
        return errors.New ("Passwords do not match");
    }

    user.Passhash = createPasshash ( pass1 );
    return nil;
}
