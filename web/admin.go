// handlers for /admin/*

package web

import (
    "strconv"
    "net/http"
    "codeberg.org/snaums/mvoCI/core"
    "codeberg.org/snaums/mvoCI/auth"

    "github.com/labstack/echo/v4"
)

// represents a modulename and a version
type iversions struct {
    Title string            // the version name or title
    Value string            // the version of the module or part
}
// represents a headline on the info page and its modules and versions
type versionStruct struct {
    Name string             // headline
    Versions []iversions    // list of versions
}

// /admin/auth
// list all authentication modules
func AdminAuthHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    if user.Superuser == false {
        return HTTPError ( 401, ctx );
    }

    provider := auth.GetAuthProvider ()
    return ctx.Render ( http.StatusOK, "admin_auth", echo.Map{
        "navPage": "admin",
        "user": user,
        "provider": provider,
    })
}

// /admin/auth/instance/:id
// TODO
func AdminAuthInstanceHandler ( ctx echo.Context ) error {
    return nil
}

// /admin/auth/instance/new/:name
// TODO
func AdminAuthInstanceNewHandler ( ctx echo.Context ) error {
    return nil
}

// /admin/info
// prints some information about the mvoCI server and its dependencies
func AdminInfoHandler ( ctx echo.Context) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    if user.Superuser == false {
        return HTTPError ( 401, ctx );
    }

    Versions := []versionStruct{
        versionStruct {
            Name: "mvoCI",
            Versions: []iversions {
                {"Version", core.Version },
                {"Build Time", core.BuildTime },
                {"Compiler", core.Compiler },
                {"Git Hash", core.GitHash },
            },
        },
        versionStruct {
            Name: "Dependencies",
            Versions: []iversions {
                {"Echo", echo.Version },
                // TODO: gorm.io
                // TODO: totp
            },
        },
    }

    return ctx.Render ( http.StatusOK, "admin_info", echo.Map{
        "navPage": "admin",
        "user": user,
        "Versions": Versions,
    })
}

// /admin/webhooklog and /admin/webhooklog/:repoid
// renders a listing of the received webhook events
func WebHookLogHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    if user.Superuser == false {
        return HTTPError ( 401, ctx );
    }

    var r core.Repository
    var repoID int
    repoID, err := strconv.Atoi ( ctx.Param("repoid") )
    if err == nil {
        s.db.Where ("id = ?", repoID ).Find( &r );
        if r.ID > 0 {
            var WebHookLogs []core.WebHookLog
            s.db.Preload("Repository").Where("repository_id = ?", r.ID).Order("created_at DESC").Find ( &WebHookLogs )
            for i, _ := range WebHookLogs {
                s.db.Preload("Repository").Model( WebHookLogs[i] );
            }
            return ctx.Render ( http.StatusOK, "webhooklog", echo.Map{
                "navPage": "admin",
                "repo": r,
                "user": user,
                "webhooklogs": WebHookLogs,
            })
        } else {
            return HTTPError ( 404, ctx );
        }
    }

    var WebHookLogs []core.WebHookLog
    s.db.Preload("Repository").Order("created_at DESC").Find ( &WebHookLogs );
    //for i, _ := range WebHookLogs {
    //    s.db.Model( WebHookLogs[i] ).Related ( &WebHookLogs[i].Repository );
    //}

    return ctx.Render ( http.StatusOK, "webhooklog", echo.Map{
        "navPage": "admin",
        "user": user,
        "webhooklogs": WebHookLogs,
    })
}

// admin/webhooklog/detail/:id
// renders a given webhook event with the response and request
func WebHookLogDetailHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    if user.Superuser == false {
        return HTTPError ( 401, ctx );
    }

    var id int
    id, err := strconv.Atoi ( ctx.Param( "id" ))
    if err != nil {
        return ctx.Redirect (http.StatusFound, s.cfg.HttpSubUrl+"admin/webhooklog")
    }

    var whlog core.WebHookLog
    s.db.Preload("Repository").Preload("Build").First ( &whlog, id );

    return ctx.Render ( http.StatusOK, "webhooklog_view", echo.Map{
        "navPage": "admin",
        "user": user,
        "whlog": whlog,
    })
}
