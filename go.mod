module codeberg.org/snaums/mvoCI

go 1.16

require (
	github.com/boombuler/barcode v1.0.1 // indirect
	github.com/denisenkom/go-mssqldb v0.12.0 // indirect
	github.com/foolin/goview v0.3.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/jackc/pgx/v4 v4.15.0 // indirect
	github.com/labstack/echo/v4 v4.6.3
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-sqlite3 v1.14.11 // indirect
	github.com/pquerna/otp v1.3.0
	golang.org/x/crypto v0.0.0-20220210151621-f4118a5b28e2
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
	golang.org/x/time v0.0.0-20220210224613-90d013bbcef8 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gorm.io/driver/mysql v1.2.3
	gorm.io/driver/postgres v1.2.3
	gorm.io/driver/sqlite v1.2.6
	gorm.io/driver/sqlserver v1.2.1
	gorm.io/gorm v1.22.5
)
