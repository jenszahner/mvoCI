
function copyToClipboard ( event ) {
    var ifield = document.getElementById(this.dataset.copyid);
    var ta = document.createElement("textarea");
    ta.style.position = 'fixed';
    ta.style.top = 0;
    ta.style.left = 0;
    ta.style.width = '2em';
    ta.style.height = '2em';
    ta.style.padding = 0;
    ta.style.border = 'none';
    ta.style.outline = 'none';
    ta.style.boxShadow = 'none';

    ta.style.background = 'transparent';
    ta.value = ifield.value;
    document.body.appendChild ( ta );
    ta.focus();
    ta.select();
    var ret = document.execCommand("copy");
    document.body.removeChild ( ta );

    if ( ret ) {
        this.classList.remove("fa-clipboard");
        this.classList.add("fa-check");
        this.classList.add("copybtn-success");
    } else {
        this.classList.remove("fa-clipboard");
        this.classList.add("fa-times");
        this.classList.add("copybtn-failed");
    }
}

document.addEventListener("DOMContentLoaded", onDomLoaded );

function onDomLoaded () {
    var copybtns = document.getElementsByClassName("copybtn");
    for ( var i = 0; i < copybtns.length; i ++ ) {
        copybtns[i].addEventListener ( "click", copyToClipboard );
    }
}

function modal_show ( id ) {
    document.getElementById (id).classList.add ("modal_visible");
}

function modal_hide ( id ) {
    document.getElementById (id).classList.remove ("modal_visible");
}

function repo_result ( res ) {
    var pblc = ""
    var out = '<a class="block-link" href="'+ window.mvoConfig.AppSubUrl +'repo/view/' + res.ID + '"><span class="block"><span class="left-side">';
    out += '<span class="block-item block-icon"><i class="fa fa-folder"></i></span>'
    out += '<span class="block-item"><span class="block-top-item';
    if ( res.Public == true ) {
        out += "string"
        pblc = " (PUBLIC)";
    }
    out +='">' + res.Name + pblc + '</span><span class="block-bottom-item">' + res.CloneUrl + '</span></span></span></span></a>';
    return out;
}

function build_noresults () {
    return '<div class="messages info"><span class="icon"><i class="fa fa-info-circle"></i></span><div>No builds found.</div></div>';
}

function repo_noresults () {
    return '<div class="messages info"><span class="icon"><i class="fa fa-info-circle"></i></span><div>No repositories found.</div></div>';
}

function build_status ( s ) {
    if ( s == "started" ) {
        return "<i class=\"fa fa-cog fa-spin\" title=\"Building\"></i>"
    } else if ( s == "enqueued" ) {
        return "<i class=\"fa fa-clock-o\" title=\"Queued\"></i>"
    } else if ( s == "failed"  || s == "Failed" ) {
        return "<i class=\"fa fa-times-circle\" title=\"Error\"></i>"
    } else if ( s == "finished" || s == "Success" ) {
        return "<i class=\"fa fa-check-circle\" title=\"Success\"></i>"
    } else {
        return "<i class=\"fa fa-question-circle\" title=\"Unknown\"></i>"
    }
}

function build_result ( res ) {
    var out = '<div class="block-link"><span class="block"><span class="left-side"><span class="block-item block-icon">' + build_status (res.Status) + '</span>';
    out += '<span class="block-item"><span class="block-top-item">';
    if ( res.CommitMessage.length > 0 ) {
        out += res.CommitMessage;
    } else {
        out += "<i>No Commit message</i>"
    }
    out +='</span><span class="block-bottom-item block-fa">' + res.CommitSha + '</span></span></span>';
    out += '<span class="right-side"><span class="block-item"><span class="block-top-item block-fa"><i class="fa fa-calendar-o"></i>' + res.StartedAt +'</span>';
    out += '<span class="block-bottom-item block-fa"><i class="fa fa-calendar-check-o"></i>';
    if ( res.Status == "failed" || res.Status == "finished" ) {
        out += res.FinishedAt;
    }
    out += '</span></span><span class="block-item dashboard-icons">';
    if ( res.Zip.length > 0 ) {
        out += '<a href="'+ window.mvoConfig.AppSubUrl +'zip/' + res.Zip + '" title="Download ZIP" class="fa fa-download"></a>'
    }
    out += '<a href="'+ window.mvoConfig.AppSubUrl +'build/' + res.ID + '" title="View Build" class="fa fa-chevron-right"></a></span></span></span></div>';
    return out;
}

function repo_search_toggle () {
    document.querySelector (".repo_search_btn").classList.add ("hidden");
    document.querySelector (".repo_search_field").classList.remove ("hidden");
    document.querySelector ("#repo_search_bar").focus();
}

function build_search_toggle () {
    document.querySelector (".build_search_btn").classList.add ("hidden");
    document.querySelector (".build_search_field").classList.remove ("hidden");
    document.querySelector ("#build_search_bar").focus();
}

function build_search ( value, results ) {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
        if ( xhr.status == 200 ) {
            out = ""
            r = JSON.parse ( xhr.responseText );
            for ( var i = 0; i < r.length; i++ ) {
                out += build_result ( r[i] );
            }

            document.querySelector ( results ).innerHTML = out;
        } else {
            console.warn ( "build api call failed: ", xhr.status, xhr.responseText );
        }
    };
    var id = document.querySelector ("#build_search_repo_id").value;
    xhr.open("GET", location.protocol + "//" + location.host + window.mvoConfig.AppSubUrl + "api/v1/build/list?name=" + value + "&id="+id, true);
    xhr.send();
}

function repo_search ( value, results ) {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
        if ( xhr.status == 200 ) {
            out = ""
            r = JSON.parse ( xhr.responseText );
            for ( var i = 0; i < r.length; i++ ) {
                out += repo_result ( r[i] );
            }

            document.querySelector ( results ).innerHTML = out;
        } else {
            console.warn ( "build api call failed: ", xhr.status, xhr.responseText );
        }
    };
    xhr.open("GET", location.protocol + "//" + location.host + window.mvoConfig.AppSubUrl + "api/v1/repo/list?name=" + value, true);
    xhr.send();
}

function repo_search_arm ( searchbar, results ) {
    document.querySelector ( searchbar ).addEventListener("keyup", function () {
        repo_search ( this.value, results );
    });
}

function build_search_arm ( searchbar, results ) {
    document.querySelector ( searchbar ).addEventListener("keyup", function () {
        build_search ( this.value, results );
    });
}

function build_refresh ( bid, interval ) {
    if ( bid <= 0 ) {
        return;
    }
    if ( interval <= 1000 ) {
        interval = 1000;
    }

    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
        if ( xhr.status == 200 ) {
            b = JSON.parse ( xhr.responseText );
            if ( b.Status == "finished" || b.Status == "failed" ) {
                location.reload();
            } else {
                window.setTimeout ( function () { build_refresh ( bid, interval ) }, interval );
                if ( window.mvoConfig.LiveLog == "true" ) {
                    build_log_refresh(bid);
                }
            }
        } else {
            console.warn ( "build api call failed: ", xhr.status, xhr.responseText );
        }
    };
    xhr.open("GET", location.protocol + "//" + location.host + window.mvoConfig.AppSubUrl + "api/v1/build?id=" + bid, true); // TODO: HttpSubUrl... As var in master layout...
    xhr.send();
}

var build_log_oldlength = 0;
function build_log_refresh(bid) {
    if ( bid <= 0 ) {
        return
    }

    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
        if ( xhr.status == 200 ) {
            b = JSON.parse ( xhr.responseText );
            if ( b.Log.length > build_log_oldlength ) {
                document.querySelector("pre.build-log > code").innerText = b.Log
                build_log_oldlength = b.Log.length
            }
        } else {
            console.warn ( "build log api call failed: ", xhr.status, xhr.responseText );
        }
    };
    xhr.open("GET", location.protocol + "//" + location.host + "/api/v1/build/log?id=" + bid, true);
    xhr.send();
}

function build_adv () {
    modal_show ("build_adv")
}

function build_adv_close () {
    modal_hide ( "build_adv");
}

function build_doAdv () {
    var branch = document.getElementById ("build_branch").value;
    var hash = document.getElementById ("build_hash").value;
    var id = document.getElementById ("build_id").value;
    var event = document.getElementById ("build_event").options[document.getElementById ("build_event").selectedIndex].value;

    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
        if ( xhr.status == 200 ) {
            b = JSON.parse ( xhr.responseText );
            if (b.ID >= 0) {
                location.href = location.protocol + "//" + location.host + window.mvoConfig.AppSubUrl + "build/"+b.ID
            } else {
                console.warn ("repo/build api call failed", xhr.status, xhr.responseText );
            }
        } else {
            console.warn ( "repo/build api call failed: ", xhr.status, xhr.responseText );
        }
    };
    xhr.open("POST", location.protocol + "//" + location.host + window.mvoConfig.AppSubUrl + "api/v1/repo/build?id=" + id+"&branch="+branch+"&hash="+hash+"&event="+event, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(build_doAdv_generateArguments());
}

function build_doAdv_generateArguments() {
    var paramCount = document.getElementById("build_arguments").querySelector("tbody").rows.length;
    var arguments = "";
    for (var i = 0; i < paramCount; i++) {
        var key = document.getElementById ("build_argument_name_"+i).value;
        var value = document.getElementById ("build_argument_value_"+i).value;
        if (key.trim() != "") {
            arguments += key.trim()+'='+value.trim()+'&';
        }
    }
    return arguments.slice(0,-1);
}

function build_doAdv_addArgument() {
	var table = document.getElementById("build_arguments");
    var idx = table.querySelector("tbody").rows.length;
	var newRow = table.getElementsByTagName('tbody')[0].insertRow();
	newRow.innerHTML = '<tr>' +
							'<td><input type="text" name="build_argument_name['+idx+']" id="build_argument_name_'+idx+'" placeholder="key"></td>' +
					   		'<td><input type="text" name="build_argument_value['+idx+']" id="build_argument_value_'+idx+'" placeholder="value"></td>'+
					   '</tr>';
}
